package com.jlexdev.lab02

import android.content.Intent
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_data.*

class DataActivity : AppCompatActivity() {

    private var name = ""
    private var years = ""
    private var months = ""
    private var vaccines = ""
    private var imagePet = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_data)

        // Receive Pet's data
        val bundle: Bundle? = intent.extras
        bundle?.let {
            name = it.getString("key_name", "")
            years = it.getString("key_years", "")
            months = it.getString("key_months", "")
            vaccines = it.getString("key_vaccines", "")
            imagePet = it.getInt("key_image_pet", 0)
        }

        // Setup Toolbar
        setSupportActionBar(toolbar)
        supportActionBar?.let {
            it.setDisplayHomeAsUpEnabled(true)
            it.setDisplayShowHomeEnabled(true)
            it.setHomeAsUpIndicator(R.drawable.ic_arrow_back)
        }

        toolbarLayout.apply {
            title = name
            setCollapsedTitleTextColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
            setExpandedTitleColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
        }


        // Print Age
        when {
            years == "0" -> {
                tvAge.text = "$months meses"
            }
            months == "0" || months == "00" -> {
                tvAge.text = "$years años"
            }
            else -> {
                tvAge.text = "$years años · $months meses"
            }
        }

        // Set Image of Pet's type
        ivPet.setImageResource(imagePet)

        // Show Vaccines selected
        tvVaccine.text =  vaccines
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}