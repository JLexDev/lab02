package com.jlexdev.lab02

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Show Vaccines by pet
        var imagePet = 0
        rgPetType.setOnCheckedChangeListener { _, _ ->
            when {
                rbCat.isChecked -> {
                    imagePet = R.drawable.img_cat
                    chbVaccine1.text = "Panleucopenia"
                    chbVaccine2.text = "Rinotraqueitis"
                    chbVaccine3.text = "Calicivirus"
                    chbVaccine4.text = "Leucemia"
                }
                rbRabbit.isChecked -> {
                    imagePet = R.drawable.img_rabbit
                    chbVaccine1.text = "Mixomatosis"
                    chbVaccine2.text = "Hemorragia vírica"
                    chbVaccine3.text = "Pasteurelosis"
                    chbVaccine4.text = "Sarna"
                }
                else -> {
                    imagePet = R.drawable.img_dog
                    chbVaccine1.text = "Distemper"
                    chbVaccine2.text = "Parvovirus"
                    chbVaccine3.text = "Parainfluenza"
                    chbVaccine4.text = "Rabia"
                }
            }
        }


        // Register
        btnRegister.setOnClickListener {

            // Validations
            val name = etName.text.toString()
            val years = etYears.text.toString()
            val months = etMonths.text.toString()

            if (name.isEmpty()) {
                this.message("Ingrese nombre")
                return@setOnClickListener
            }

            if (years.isEmpty()) {
                this.message("Ingrese años")
                return@setOnClickListener
            }

            if (months.isEmpty()) {
                this.message("Ingrese meses")
                return@setOnClickListener
            }

            if (months.toInt() >= 12) {
                this.message("Ingrese edad correcta")
                return@setOnClickListener
            }

            if (years == "0") {
                if (months == "0" || months == "00") {
                    this.message("Ingrese edad correcta")
                    return@setOnClickListener
                }
            }

            if (rgPetType.checkedRadioButtonId == -1) {
                this.message("Seleccione tipo de mascota")
                return@setOnClickListener
            }


            // Select Vaccines
            var isSelected = false
            var vaccines = ""

            if (chbVaccine1.isChecked) {
                vaccines += "· ${chbVaccine1.text}\n\n"
                isSelected = true
            }

            if (chbVaccine2.isChecked) {
                vaccines += "· ${chbVaccine2.text}\n\n"
                isSelected = true
            }

            if (chbVaccine3.isChecked) {
                vaccines += "· ${chbVaccine3.text}\n\n"
                isSelected = true
            }

            if (chbVaccine4.isChecked) {
                vaccines += "· ${chbVaccine4.text}\n\n"
                isSelected = true
            }

            if (!isSelected) {
                vaccines = "$name no cuenta con vacunas"
            }


            // Send Pet's data
            val bundle = Bundle()
            bundle.apply {
                putString("key_name", name)
                putString("key_years", years)
                putString("key_months", months)
                putString("key_vaccines", vaccines)
                putInt("key_image_pet", imagePet)
            }


            // Go to Data Activity
            val intent = Intent(this, DataActivity::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)
        }
    }


    /**
     * Toast Message
     * */
    fun Context.message(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }

}